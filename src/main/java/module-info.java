module no.ntnu.idatt2002.a4.cardgame {
  requires javafx.controls;
  requires javafx.fxml;


  opens no.ntnu.idatt2002.a4.cardgame to javafx.fxml;
  exports no.ntnu.idatt2002.a4.cardgame;
  exports no.ntnu.idatt2002.a4.cardgame.controller;
  opens no.ntnu.idatt2002.a4.cardgame.controller to javafx.fxml;
  exports no.ntnu.idatt2002.a4.cardgame.model;
  opens no.ntnu.idatt2002.a4.cardgame.model to javafx.fxml;
}