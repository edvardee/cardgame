package no.ntnu.idatt2002.a4.cardgame.model;

import java.util.*;

public class DeckOfCards {
  private final HashMap<Integer, PlayingCard> deck;
  private final char[] suits = {'H', 'D', 'S', 'C'};

  public DeckOfCards() {
    deck = new HashMap<>();
    int iteration = 0;
    for (char suit : suits) {
      for (int i = 1; i < 14; i++) {
        iteration++;
        deck.put(iteration, new PlayingCard(suit, i));
      }
    }
  }
  public HashMap<Integer, PlayingCard> getDeck() {
    return deck;
  }
  public List<PlayingCard> dealHand(int numberOfCards) throws IllegalArgumentException {
    if (numberOfCards < 1 || numberOfCards > 52) {
      throw new IllegalArgumentException("Number of cards must be between 1 and 52");
    }
    Random random = new Random();
    List<PlayingCard> hand = new ArrayList<>(numberOfCards);
    HashSet<Integer> dealtKeys = new HashSet<>();
    while (hand.size() < numberOfCards) {
      int randomKey = random.nextInt(1, this.deck.size());
      while (dealtKeys.contains(randomKey)) {
        randomKey = random.nextInt(1, this.deck.size());
      }
      hand.add(deck.get(randomKey));
      dealtKeys.add(randomKey);
    }
    return hand;
  }
}
