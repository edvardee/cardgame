package no.ntnu.idatt2002.a4.cardgame.controller;

import javafx.scene.image.Image;
import no.ntnu.idatt2002.a4.cardgame.model.DeckOfCards;
import no.ntnu.idatt2002.a4.cardgame.model.PlayingCard;


import java.util.ArrayList;
import java.util.List;

public class CardGameController {
  private final DeckOfCards deck = new DeckOfCards();
  private List<PlayingCard> playingCards;

  public List<Image> getImages() {
    playingCards = deck.dealHand(5);

    List<Image> images = new ArrayList<>();
    for (PlayingCard card : playingCards) {
      images.add(card.getCardImage());
    }
    return images;
  }

  public List<PlayingCard> getPlayingCards() {
    return playingCards;
  }

  public void setPlayingCards(List<PlayingCard> playingCards) {
    this.playingCards = playingCards;
  }
  public boolean isFlush() {
    return this.playingCards.stream().allMatch(card -> card.getSuit() == this.playingCards.getFirst().getSuit());
  }
  public int sumOfFaces() {
    return this.playingCards.stream().mapToInt(PlayingCard::getFace).sum();
  }
  public String cardsOfHearts() {
    return this.playingCards.stream().filter(card -> card.getSuit() == 'H').map(PlayingCard::getAsString).reduce("", (a, b) -> a + " " + b);
  }
  public boolean queenOfSpades() {
    return this.playingCards.stream().anyMatch(card -> card.getSuit() == 'S' && card.getFace() == 12);
  }
}
