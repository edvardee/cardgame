package no.ntnu.idatt2002.a4.cardgame;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import no.ntnu.idatt2002.a4.cardgame.controller.CardGameController;
import no.ntnu.idatt2002.a4.cardgame.view.BottomBox;
import no.ntnu.idatt2002.a4.cardgame.view.CenterBox;
import no.ntnu.idatt2002.a4.cardgame.view.RightBox;

import java.io.IOException;

public class CardGameApplication extends Application {

  @Override
  public void start(Stage primaryStage) throws IOException {
    BorderPane root = new BorderPane();
    Scene scene = new Scene(root, 1400, 900);
    //


    CenterBox centerBox= new CenterBox();
    HBox centerBackground = centerBox.getCenterBackground();

    BottomBox bottomBox = new BottomBox();
    HBox bottomB = bottomBox.getBottomBox();

    RightBox rightBox = new RightBox();
    VBox rightB = rightBox.getRightBox();
    CardGameController cardGameController = new CardGameController();

    rightBox.getDealHandButton().setOnAction(e -> {
      centerBox.setCenterBox(cardGameController.getImages());
    });
    rightBox.getCheckHandButton().setOnAction(e -> {
      if (cardGameController.getPlayingCards() != null) {
        boolean isFlush = cardGameController.isFlush();
        int sumOfFaces = cardGameController.sumOfFaces();
        String cardsOfHearts = cardGameController.cardsOfHearts();
        boolean queenOfSpades = cardGameController.queenOfSpades();
        bottomBox.setBottomBoxes(isFlush, queenOfSpades, cardsOfHearts, sumOfFaces);
      }
    });
    root.setRight(rightB);
    root.setCenter(centerBackground);


    root.setBottom(bottomB);
    primaryStage.setTitle("Card Game");
    primaryStage.setScene(scene);
    primaryStage.show();

  }

  public static void main(String[] args) {
    launch();
  }
}