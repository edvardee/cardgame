package no.ntnu.idatt2002.a4.cardgame.view;

import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;
import javafx.scene.shape.Rectangle;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

public class BottomBox {
  private final HBox bottomBox = new HBox();
  private Rectangle sumOfFacesRectangle;
  private StackPane sumOfFacesStackPane;
  private Rectangle cardsOfHeartsRectangle;

  private StackPane cardsOfHeartsStackPane;
  private Rectangle cardsFlushRectangle;
  private StackPane cardsFlushStackPane;
  private Rectangle queenOfSpadesRectangle;
  private StackPane queenOfSpadesStackPane;

  private static void setRectangleStyle(Rectangle rectangle){
    rectangle.setWidth(105);
    rectangle.setHeight(20);
    rectangle.setArcHeight(20);
    rectangle.setArcWidth(20);
    rectangle.setStyle("-fx-fill: #eeffdd; -fx-stroke: black; -fx-stroke-width: 2;");
  }
  public BottomBox () {
    bottomBox.setSpacing(20);

    Label sumOfFacesLabel = new Label("Sum of faces: ");
    sumOfFacesLabel.setStyle("-fx-text-fill: white;");
    sumOfFacesRectangle = new Rectangle();
    sumOfFacesStackPane = bottomStackPane("0 ", sumOfFacesRectangle);

    Label cardsOfHeartsLabel = new Label("Cards of hearts: ");
    cardsOfHeartsLabel.setStyle("-fx-text-fill: white;");
    cardsOfHeartsRectangle = new Rectangle();
    cardsOfHeartsStackPane = bottomStackPane("H# H# ", cardsOfHeartsRectangle);

    Label cardsFlushLabel = new Label("Cards flush: ");
    cardsFlushLabel.setStyle("-fx-text-fill: white;");
    cardsFlushRectangle = new Rectangle();
    cardsFlushStackPane = bottomStackPane("YES/NO ", cardsFlushRectangle);

    Label queenOfSpadesLabel = new Label("Queen of spades: ");
    queenOfSpadesLabel.setStyle("-fx-text-fill: white;");
    queenOfSpadesRectangle = new Rectangle();
    queenOfSpadesStackPane = bottomStackPane("YES/NO ", queenOfSpadesRectangle);

    VBox bottomGroup = new VBox(20);
    bottomGroup.setAlignment(Pos.CENTER);

    VBox bottomGroup2 = new VBox(20);
    bottomGroup2.setAlignment(Pos.CENTER);

    bottomGroup.getChildren().addAll(sumOfFacesLabel,sumOfFacesStackPane,
            cardsOfHeartsLabel, cardsOfHeartsStackPane);
    bottomGroup2.getChildren().addAll(cardsFlushLabel, cardsFlushStackPane,
            queenOfSpadesLabel, queenOfSpadesStackPane);

    bottomBox.getChildren().addAll(bottomGroup, bottomGroup2);
    bottomBox.setMinHeight(200);
    bottomBox.setAlignment(Pos.CENTER);
    bottomBox.setStyle("-fx-background-color: #252526; -fx-border-color: black; -fx-border-width: 2;");
  }

  public HBox getBottomBox() {
    return bottomBox;
  }


  private static StackPane bottomStackPane(String rectangleText, Rectangle rectangle){
    Label label = new Label(rectangleText);
    label.setStyle("-fx-text-fill: white;");
    StackPane stackPane = new StackPane();
    setRectangleStyle(rectangle);
    rectangle.setStyle("-fx-fill: #0d2d3a; -fx-stroke: black; -fx-stroke-width: 2;");
    stackPane.getChildren().addAll(rectangle, label);
    return stackPane;
  }
  public void setBottomBoxes(boolean isFlush, boolean queenOfSpades, String cardsOfHearts, int sumOfFaces){
    cardsOfHeartsStackPane.getChildren().clear();
    sumOfFacesStackPane.getChildren().clear();
    cardsFlushStackPane.getChildren().clear();
    queenOfSpadesStackPane.getChildren().clear();
    if(isFlush) {
      Label cardsFlushLabel = new Label("YES ");
      cardsFlushLabel.setStyle("-fx-text-fill: white;");
      cardsFlushStackPane.getChildren().addAll(cardsFlushRectangle, cardsFlushLabel);
    } else {
      Label cardsFlushLabel = new Label("NO ");
      cardsFlushLabel.setStyle("-fx-text-fill: white;");
      cardsFlushStackPane.getChildren().addAll(cardsFlushRectangle, cardsFlushLabel);
    }
    if (queenOfSpades) {
      Label queenOfSpadesLabel = new Label("YES ");
      queenOfSpadesLabel.setStyle("-fx-text-fill: white;");
      queenOfSpadesStackPane.getChildren().addAll(queenOfSpadesRectangle, queenOfSpadesLabel);
    } else {
      Label queenOfSpadesLabel = new Label("NO ");
      queenOfSpadesLabel.setStyle("-fx-text-fill: white;");
      queenOfSpadesStackPane.getChildren().addAll(queenOfSpadesRectangle, queenOfSpadesLabel);
    }
    Label sumOfFacesLabel = new Label(Integer.toString(sumOfFaces));
    sumOfFacesLabel.setStyle("-fx-text-fill: white;");
    sumOfFacesStackPane.getChildren().addAll(sumOfFacesRectangle, sumOfFacesLabel);
    Label cardOfHeartsLabel = new Label(cardsOfHearts);
    cardOfHeartsLabel.setStyle("-fx-text-fill: white;");
    cardsOfHeartsStackPane.getChildren().addAll(cardsOfHeartsRectangle, cardOfHeartsLabel);

  }
}
