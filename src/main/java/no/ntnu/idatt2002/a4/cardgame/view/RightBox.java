package no.ntnu.idatt2002.a4.cardgame.view;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;

public class RightBox {
  private final VBox rightBox = new VBox(20);
  private Button dealHandButton = new Button("Deal Hand");
  private Button checkHandButton = new Button("Check Hand");
  private final String buttonStyle = "-fx-text-fill: white; -fx-font: 22 arial; -fx-background-color: #0d2d3a; -fx-border-radius: 20; -fx-background-insets: 2; -fx-background-radius: 20; -fx-border-color: black; -fx-border-width: 2;";
  public RightBox() {
    rightBox.setStyle("-fx-background-color: #252526");
    rightBox.setAlignment(Pos.CENTER_LEFT);
    rightBox.setPadding(new Insets(20));
    dealHandButton.setStyle(buttonStyle);
    checkHandButton.setStyle(buttonStyle);
    rightBox.getChildren().addAll(dealHandButton, checkHandButton);
  }
  public VBox getRightBox() {
    return rightBox;
  }
  public Button getDealHandButton() {
    return dealHandButton;
  }
  public Button getCheckHandButton() {
    return checkHandButton;
  }

}
