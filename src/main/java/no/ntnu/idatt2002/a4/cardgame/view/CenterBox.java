package no.ntnu.idatt2002.a4.cardgame.view;

import javafx.geometry.Pos;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

import java.util.List;

public class CenterBox {
  private final HBox centerBackground = new HBox();
  private HBox cardDisplayBox = new HBox();
  public CenterBox() {

    cardDisplayBox.setMaxHeight(400);
    cardDisplayBox.setPrefWidth(1180);
    cardDisplayBox.setSpacing(20);
    cardDisplayBox.setAlignment(Pos.CENTER);
    cardDisplayBox.setStyle("-fx-background-color: #0d2d3a; -fx-border-color: black; -fx-border-width: 2; " +
            "-fx-background-insets: 2; -fx-border-radius: 20; -fx-background-radius: 20");
    Image cardImage = new Image("file:src/main/resources/images/empty.png");
    for (int i = 0; i < 5; i++) {
      ImageView cardImageView = new ImageView(cardImage);
      cardImageView.setPreserveRatio(true);
      cardImageView.setFitWidth(200);
      VBox cardBox = new VBox();
      cardBox.getChildren().add(cardImageView);
      cardBox.setAlignment(Pos.CENTER);
      cardDisplayBox.getChildren().add(cardBox);

    }
    cardDisplayBox.setAlignment(Pos.CENTER);
    centerBackground.getChildren().add(cardDisplayBox);

    centerBackground.setStyle("-fx-background-color: #1d1d1d");
    centerBackground.setAlignment(Pos.CENTER);

  }
  public HBox getCenterBackground() {
    return centerBackground;
  }
  public void setCenterBox(List<Image> cardImages) {
    cardDisplayBox.getChildren().clear();
    for (Image cardImage : cardImages) {
      ImageView cardImageView = new ImageView(cardImage);
      cardImageView.setPreserveRatio(true);
      cardImageView.setFitWidth(200);
      VBox cardBox = new VBox();
      cardBox.getChildren().add(cardImageView);
      cardBox.setAlignment(Pos.CENTER);
      cardDisplayBox.getChildren().add(cardBox);
    }

    cardDisplayBox.setAlignment(Pos.CENTER);
    centerBackground.setStyle("-fx-background-color: #1d1d1d");
    centerBackground.setAlignment(Pos.CENTER);

  }
}
