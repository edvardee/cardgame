package no.ntnu.idatt2002.a4.cardgame.controller;

import javafx.scene.layout.BorderPane;
import no.ntnu.idatt2002.a4.cardgame.model.PlayingCard;
import org.junit.jupiter.api.*;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class CardGameControllerTest {
  private final CardGameController controller = new CardGameController();
  @BeforeAll
  public static void setUpClass() throws Exception {
    try {
        javafx.application.Platform.startup(() -> {
        });

    } catch (IllegalStateException e) {
      // ignore, already started

    }
  }
  private final List<PlayingCard> playingCards = setPlayingCards('H', 'H', 'H', 'H', 'H');
  private final List<PlayingCard> playingCards1 = setPlayingCards('D', 'D', 'D', 'D', 'D');
  private final List<PlayingCard> playingCards2 = setPlayingCards('H', 'H', 'H', 'H', 'S');

  private final List<PlayingCard> playingCards3 = setPlayingCards('S', 13, 12, 11, 10, 9);

  @Nested
  @DisplayName("Test stream methods")
  public class Streams {
    @Nested
    @DisplayName("Test method isFlush")
    class IsFlush {
      @Test
      @DisplayName("Test if the method isFlush returns true")
      void isFlush() {
        controller.setPlayingCards(playingCards);
        assertTrue(controller.isFlush());
        controller.setPlayingCards(playingCards1);
        assertTrue(controller.isFlush());
      }

      @Test
      @DisplayName("Test if the method isFlush returns false")
      void isFlushFalse() {
        controller.setPlayingCards(playingCards2);
        assertFalse(controller.isFlush());
      }
    }
    @Nested
    @DisplayName("Test method queenOfSpades")
    class QueenOfSpades {
      @Test
      @DisplayName("Test if the method queenOfSpades returns true")
      void queenOfSpades() {
        controller.setPlayingCards(playingCards3);
        assertTrue(controller.queenOfSpades());
      }

      @Test
      @DisplayName("Test if the method queenOfSpades returns false")
      void queenOfSpadesFalse() {
        controller.setPlayingCards(playingCards1);
        assertFalse(controller.queenOfSpades());
      }
    }
    @Nested
    @DisplayName("Test method sumOfFaces")
    class SumOfFaces {
      @Test
      @DisplayName("Test if the method sumOfFaces returns true")
      void sumOfFaces() {
        controller.setPlayingCards(playingCards);
        assertEquals(15, controller.sumOfFaces());
      }

      @Test
      @DisplayName("Test if the method sumOfFaces returns false")
      void sumOfFacesFalse() {
        controller.setPlayingCards(playingCards1);
        assertEquals(15, controller.sumOfFaces());
      }
    }
    @Nested
    @DisplayName("Test method cardsOfHearts")
    class CardsOfHearts {
      @Test
      @DisplayName("Test if the method cardsOfHearts returns true")
      void cardsOfHearts() {
        controller.setPlayingCards(playingCards);
        assertEquals(" H1 H2 H3 H4 H5", controller.cardsOfHearts());
      }

      @Test
      @DisplayName("Test if the method cardsOfHearts returns false")
      void cardsOfHeartsFalse() {
        controller.setPlayingCards(playingCards1);
        assertNotEquals(" H1 H2 H3 H4 H5", controller.cardsOfHearts());
      }
    }
  }

  private List<PlayingCard> setPlayingCards(char ... otherSuits) {
    List<PlayingCard> playingCards = new ArrayList<>();
    int face = 0;
    for (char suit : otherSuits) {
      face++;
      playingCards.add(new PlayingCard(suit, face));
    }
    return playingCards;
  }
  private List<PlayingCard> setPlayingCards(char suit, int ... otherFaces) {
    List<PlayingCard> playingCards = new ArrayList<>();
    for (int face : otherFaces) {
      playingCards.add(new PlayingCard(suit, face));
    }
    return playingCards;
  }
}