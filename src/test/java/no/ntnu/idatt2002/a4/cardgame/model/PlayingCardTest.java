package no.ntnu.idatt2002.a4.cardgame.model;

import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;

class PlayingCardTest {
  private final char suit = 'H';
  private final int face = 1;
  private final PlayingCard card = new PlayingCard(suit, face);

  @BeforeAll
  public static void setUpClass() throws Exception {
    try {
      javafx.application.Platform.startup(() -> {
      });

    } catch (IllegalStateException e) {
      // ignore, already started

    }
  }
  @Nested
  @DisplayName("Test accessor methods")
  public class AccessorMethods{
    @Test
    @DisplayName("Test getSuit")
    void getSuit() {
      char expected = 'H';
      assertEquals(expected, card.getSuit());
    }

    @Test
    @DisplayName("Test getFace")
    void getFace() {
      int expected = 1;
      assertEquals(expected, card.getFace());
    }

    @Test
    @DisplayName("Test getCardImage")
    void getCardImage() {
      assertNotNull(card.getCardImage());
    }
  }
  @Test
  @DisplayName("Test getSuit")
  void getSuit() {
    char expected = 'H';
    assertEquals(expected, card.getSuit());
  }

  @Test
  @DisplayName("Test getFace")
  void getFace() {
    int expected = 1;
    assertEquals(expected, card.getFace());
  }

  @Test
  @DisplayName("Test getCardImage")
  void getCardImage() {
    assertNotNull(card.getCardImage());
  }
}