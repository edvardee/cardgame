package no.ntnu.idatt2002.a4.cardgame.model;

import org.junit.jupiter.api.*;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class DeckOfCardsTest {

  @BeforeAll
  public static void setUpClass() throws Exception {
    try {
      javafx.application.Platform.startup(() -> {
      });

    } catch (IllegalStateException e) {
      // ignore, already started

    }
  }

  @Test
  void dealHand() {
    DeckOfCards deck = new DeckOfCards();
    List<PlayingCard> hand = deck.dealHand(5);
    assertEquals(5, hand.size());
  }

  @Test
  @DisplayName("Test dealHand with invalid arguments")
  void dealHandInvalid() {
    DeckOfCards deck = new DeckOfCards();
    assertThrows(IllegalArgumentException.class, () -> deck.dealHand(53));
  }
}