package no.ntnu.idatt2002.a4.cardgame;

import org.junit.jupiter.api.AfterAll;

public class JavaFXFinalizer {
  @AfterAll
  public static void tearDownClass() throws Exception {
    // Terminates the JavaFX Toolkit
    javafx.application.Platform.exit();
  }
}
